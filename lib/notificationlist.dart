import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:sign_in_flutter/notify.dart';
import 'notification.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'try.dart';
import 'process.dart';
import 'api_link.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;

// MyApp is a StatefulWidget. This allows updating the state of the
// widget when an item is removed.
class NotiList extends StatefulWidget {
  @override
  NotiListState createState() => NotiListState();
}

class NotiListState extends State<NotiList> {
  final itemsList = List<String>.generate(10, (n) => "List item ${n}");
  final dbRef = FirebaseDatabase.instance.reference().child("Child_Data");
  final globalKey = GlobalKey<ScaffoldState>();
  List<dynamic> list;
  bool done1 = false;
  void initState() {
    get_matched();
    // This is the initial data // Set it in initState because you are using a stateful widget
    super.initState();
  }

  Widget slideRightBackground() {
    return Container(
      color: Colors.green,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Icon(
              Icons.edit,
              color: Colors.white,
            ),
            Text(
              " Edit",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
        alignment: Alignment.centerLeft,
      ),
    );
  }

  Widget slideLeftBackground() {
    return Container(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: Colors.white,
            ),
            Text(
              " Delete",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: globalKey,
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MyHomePage(
                            notification_key: 0,
                          ),
                        ));
                  },
                ),
                Spacer(),
                Text(
                  "Notification Bar",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                ),
              ]),
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: Container(
            child: done1
                ? new ListView.builder(
                    shrinkWrap: true,
                    itemCount: list.length,
                    //scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return new Dismissible(
                        key: UniqueKey(),
                        background: slideLeftBackground(),
                        onDismissed: (direction) {
                          if (direction == DismissDirection.endToStart) {
                            delete_fromserver(index);
                          }
                        },
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UserNotification(
                                          searchname: list[index][1],
                                        )));
                          },
                          child: Container(
                            margin: const EdgeInsets.only(
                                top: 2.0, left: 5.0, right: 5.0, bottom: 2.0),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              //border: Border.all(color: Colors.blue),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Card(
                                  child: ListTile(
                                    leading: CircleAvatar(
                                      radius: 30,
                                      backgroundColor: Color(0xffff7f50),
                                      child: Icon(
                                        Icons.notifications,
                                        color: Colors.grey[800],
                                        size: 30.0,
                                      ),
                                    ),
                                    title: Text("New Notification"),
                                    subtitle: Text("Name: " +
                                        list[index][1] +
                                        " \nReporting Email: " +
                                        list[index][2]),
                                    trailing: Icon(
                                      Icons.arrow_forward_ios_outlined,
                                      color: Colors.green[800],
                                    ),
                                    isThreeLine: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    })
                : Center(
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                    ],
                  ))));
  }

  get_matched() async {
    var uri = Uri.parse(uri2 + '/matched?email=$email');
    var result = await http.get(uri);
    var data = json.decode(result.body);
    // setState(() {
    //   res = data['url'] as List;
    // });
    var res = data['url'] as List;

    // print(res.length);
    setState(() {
      //list = res.map<Article>((json) => Article.fromJson(json)).toList();
      list = res;
      done1 = true;
    });

    //  list = res.map<Article>((json) => Article.fromJson(json)).toList();
    // print("List Size: ${list.length}");
  }

  Future<void> delete_fromserver(int index) async {
    print(index);
    print("attempting to connecting to server......");
    final response = await http.get(
      Uri.parse(uri2 + '/delete?id=$index'),
    );
    if (response.statusCode == 200) {
      list.removeAt(index);
      globalKey.currentState.showSnackBar(new SnackBar(
        content: new Text("Item Deleted"),
      ));
    }
  }
}
