import 'package:flutter/material.dart';
import 'package:sign_in_flutter/api_link.dart';
import 'package:sign_in_flutter/childinfo.dart';
import 'package:sign_in_flutter/try.dart';

import 'FirstScreen2.dart';
import 'package:http/http.dart' as http;

import 'sign_in.dart';
import 'login_page.dart';
import 'getall.dart';
import 'notificationlist.dart';
import 'No_notification.dart';
import 'api_link.dart';

class MyHomePage extends StatefulWidget {
  final int notification_key;
  final String title;
  MyHomePage({Key key, this.title, @required this.notification_key})
      : super(key: key);

  @override
  _MyHomePageState createState() =>
      new _MyHomePageState(notification_key: this.notification_key);
}

class _MyHomePageState extends State<MyHomePage> {
  int notification_key;
  _MyHomePageState({this.notification_key});
  var response1;
  final keyIsFirstLoaded = 'is_first_loaded';

  int notification = 0;

  get_notification() async {
    var uri = Uri.parse(uri2 + 'login?email=$email');
    print("connection established.");
    var request = new http.MultipartRequest("POST", uri);
    await Future<void>.delayed(Duration(seconds: 15));
    var response = await request.send();
    response1 = await response.stream.bytesToString();
    print(response1);
    if (response1 == "true") {
      notification = 1;
      if (notification_key == 1) {
        showAlert(1);
      }
    } else {
      notification = 0;
      if (notification_key == 1) {
        showAlert(0);
      }
    }
  }

  void showAlert(int key) {
    if (key == 1) {
      Widget remindButton = FlatButton(
        child: Text("Check"),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => NotiList()));
        },
      );
      Widget cancelButton = FlatButton(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
            // prefs.setBool(keyIsFirstLoaded, false);
          });
      AlertDialog alert = AlertDialog(
        content: Text("You have new notification! "),
        actions: [
          remindButton,
          cancelButton,
        ],
      );

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
    if (key == 0) {
      Widget cancelButton = FlatButton(
          child: Text("Ok"),
          onPressed: () {
            Navigator.of(context).pop();
          });
      AlertDialog alert = AlertDialog(
        content: Text("You have NO new notification! "),
        actions: [
          cancelButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
        elevation: 14.0,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x802196F3),
        child: InkWell(
            // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null
                ? () => onTap()
                : () {
                    print('Not set yet');
                  },
            child: child));
  }

  @override
  Widget build(BuildContext context) {
    //Future.delayed(Duration.zero, () => showAlert(context));

   

    return Scaffold(
      appBar: AppBar(
        title: Row(children: <Widget>[
          IconButton(
              icon: Stack(
                children: [
                  Icon(Icons.notifications, color: Colors.black),
                  Positioned(
                      left: 16.0,
                      child: Icon(Icons.brightness_1,
                          color: Colors.red, size: 9.0)),
                ],
              ),
              onPressed: () {
                 get_notification();
                if (notification == 1) {

                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NotiList()));
                }
              }),

          Spacer(),
          // Padding(
          //   padding: EdgeInsets.all(0.0),
          //   child: new DropdownButton<String>(
          //     value: _currentScreen,
          //     iconEnabledColor: Colors.black,
          //     onChanged: (value) {
          //       // print(value);
          //       _currentScreen = value;
          //       switch (value) {
          //         case "Welcome":
          //           Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //                   builder: (context) => MyHomePage(
          //                        notification_key: 0,
          //                       )));
          //           break;
          //         case "Notifications":
          //           {
          //             if (notification == 1)
          //               Navigator.push(
          //                   context,
          //                   MaterialPageRoute(
          //                       builder: (context) => NotiList()));
          //             else
          //               Navigator.push(context,
          //                   MaterialPageRoute(builder: (context) => Nonoti()));
          //           }
          //           break;
          //       }
          //     }, //value: _currentScreen ?? null,
          //     items: _screens.map((scr) {
          //       return DropdownMenuItem<String>(
          //         value: scr,
          //         child: Text(
          //           scr,
          //           style: TextStyle(color: Colors.black, fontSize: 20),
          //         ),
          //       );
          //     }).toList(),
          //     hint: Text(name),
          //   ),
          // ),
          Spacer(),
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
            onPressed: () {
              signOutGoogle();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) {
                return LoginPage();
              }), ModalRoute.withName('/'));
            },
          ),
        ]),
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 5.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Container(
                margin: const EdgeInsets.only(
                    top: 0.0, left: 5.0, right: 5.0, bottom: 50.0),
                padding: EdgeInsets.all(150),
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  image: new AssetImage("assets/4612606.jpg"),
                  fit: BoxFit.cover,
                ))),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(),
                _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(40.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Material(
                              color: Colors.black,
                              shape: CircleBorder(),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.description,
                                    color: Colors.white, size: 30.0),
                              )),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Report',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20.0)),
                          Text('Missing Child',
                              style: TextStyle(color: Colors.black45)),
                        ]),
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => FirstScreen2(),
                    ),
                  ),
                ),
                Spacer(),
                _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(41.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Material(
                              color: Colors.black,
                              shape: CircleBorder(),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.upload_rounded,
                                    color: Colors.white, size: 30.0),
                              )),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('View',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20.0)),
                          Text('All Reports',
                              style: TextStyle(color: Colors.black45)),
                        ]),
                  ),
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => Child_Information(parentphone: '03412037275'),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
          ],
        ),
      ),
    );
    //print(key_check);
  }
}
