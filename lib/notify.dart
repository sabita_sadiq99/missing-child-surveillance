import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'package:sign_in_flutter/api_link.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'try.dart';
import 'dart:convert';
import 'api_link.dart';
import './process.dart';

import 'dart:io';

class UserNotification extends StatefulWidget {
  final String searchname;
  UserNotification({Key key, @required this.searchname}) : super(key: key);

  UserNotificationState createState() => UserNotificationState(searchname);
}

class UserNotificationState extends State<UserNotification> {
  @override
  final String searchname;
  UserNotificationState(this.searchname);
  final dbRef = FirebaseDatabase.instance.reference().child("Child_Data");
  final globalKey = GlobalKey<ScaffoldState>();
  List<dynamic> list;
  List<Map<dynamic, dynamic>> lists = [];
  List<dynamic> urls = List<dynamic>();
  List<dynamic> newurls = List<dynamic>();
  bool done1,done2 = false;
  void initState() {
    get_server_image(searchname);
    // This is the initial data // Set it in initState because you are using a stateful widget
    super.initState();
  }
  // String url =

  get_server_image(String cname) async {
    var uri = Uri.parse(uri2 + '/getreport?name=$cname');
    var result = await http.get(uri);
    var data = json.decode(result.body);
    var res = data['url'] as List;

    setState(() {
      list = res;
      done1 = true;
      if(list.length >1)
      {
        done2 =  true;
      }
    });

  }

  change_status(String name, String email, String url1) async {
    final response = await http.get(
      Uri.parse(uri2 + '/update?name=$name&&email=$email&&submit=$url1'),
    );
    if (response.statusCode == 200) {
      showAlertDialog(context);
    }
  }

  wrong_report(
    String name,
    String email,
    String url1,
  ) async {
    final response = await http.get(
      Uri.parse(uri2 + '/wrong?name=$name&&email=$email&&submit=$url1'),
    );
    if (response.statusCode == 200) {
      showAlertDialog1(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: globalKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                Text(
                  "Matched Report",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                ),
              ]),
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: FutureBuilder(
            future: dbRef.orderByChild("cname").equalTo("$searchname").once(),
            builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
              if (snapshot.hasData) {
                lists.clear();
                return 
                done1? new ListView.builder(
                    shrinkWrap: true,
                    itemCount: 1,

                    //scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        padding: EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              //  mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(height: 20.0),
                                Text(
                                  'Congratulations!',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 10.0),
                                Text(
                                  'We have found your Match',
                                  style: TextStyle(
                                    color: Colors.grey[600],
                                    fontSize: 18.0,
                                    letterSpacing: 0.2,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(height: 20.0),
                                Row(
                                  children: [
                                    Align(
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 5.0),
                                              child: Text(
                                                'Submitted Picture',
                                              ),
                                            ),
                                            SizedBox(height: 5.0),
                                            new Image.memory(
                                              base64Decode(list[0][1]),
                                              width: 100.0,
                                              height: 130.0,
                                              fit: BoxFit.cover,
                                            ),
                                          ],
                                        )),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding:
                                                EdgeInsets.only(right: 5.0),
                                            child: Text(
                                              'Top Matched Picture',
                                            ),
                                          ),
                                          SizedBox(height: 5.0),
                                          new Image.memory(
                                            base64Decode(list[1][2]),
                                            width: 100.0,
                                            height: 130.0,
                                            fit: BoxFit.cover,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 20.0),
                               done2?Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.only(left: 5.0),
                                        child: Text(
                                          'Other Matched Pictures',
                                          style: TextStyle(fontSize: 17.0),
                                        )),
                                    SizedBox(
                                        height: 80.0,
                                        child: ListView.builder(
                                            physics: ClampingScrollPhysics(),
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            itemCount:4,
                                            itemBuilder: (BuildContext context,
                                                    int index) =>
                                                Card(
                                                    child: Center(
                                                        child: Image.memory(
                                                  base64Decode(
                                                      list[index + 1][2]),
                                                  fit: BoxFit.cover,
                                                )))))
                                  ],
                                ):
                                SizedBox(
                                  height: 50,
                                ),
                                Row(
                                  children: [
                                    Spacer(),
                                    RaisedButton.icon(
                                      onPressed: () {
                                        change_status(
                                            searchname, email, list[0][0]);
                                      },
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10.0))),
                                      label: Text(
                                        'Accept',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      icon: Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                      textColor: Colors.white,
                                      splashColor: Colors.red,
                                      color: Colors.green,
                                    ),
                                    Spacer(),
                                    RaisedButton.icon(
                                      onPressed: () {
                                        print('hello');
                                        wrong_report(
                                            searchname, email, list[0][0]);
                                      },
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10.0))),
                                      label: Text(
                                        'Reject',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      icon: Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                      textColor: Colors.white,
                                      splashColor: Colors.red,
                                      color: Colors.red,
                                    ),
                                    Spacer(),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                      );
                    }):Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ));
              }
              return Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ));
            }));
  }

  showAlertDialog(BuildContext context) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (
              context,
            ) {
              // _showLoadingDialog();
              return MyHomePage(
                notification_key: 0,
              );
            },
          ),
        );
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Congratulation"),
      content: Text("Our representator will call you to verify the details "),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialog1(BuildContext context) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (
              context,
            ) {
              // _showLoadingDialog();
              return MyHomePage(
                notification_key: 0,
              );
            },
          ),
        );
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Sorry for the Wrong match"),
      content: Text("The process is still going on. Dont Lose Hope"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
