import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:sign_in_flutter/api_link.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'dart:io';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import 'package:path/path.dart' as path;
import "package:http/http.dart" as http;
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';
import 'searchpage.dart';

import 'package:validators/validators.dart';
import 'package:async/async.dart';
import 'package:path_provider/path_provider.dart';

import 'api_link.dart';

class Child_Information extends StatelessWidget {
  final parentphone;
  Child_Information({
    Key key,
    @required this.parentphone,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    //print(parentphone);
    return Scaffold(
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                Text(
                  "Child Information",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                ),
              ]),
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: Center(
            child: ChildInfo(
          parentphone: this.parentphone,
        )));
  }
}

class ChildInfo extends StatefulWidget {
  final String parentphone;
  ChildInfo({Key key, this.parentphone}) : super(key: key);

  HomeScreenState createState() =>
      HomeScreenState(parentphone: this.parentphone);
}

class HomeScreenState extends State<ChildInfo> {
  String parentphone;
  HomeScreenState({this.parentphone});
  final _cusername = TextEditingController();
  final _cage = TextEditingController();
  final _cgender = TextEditingController();
  final _cparent = TextEditingController();
  final _lastplace = TextEditingController();
  final _ceducation = TextEditingController();
  final _caddress = TextEditingController();
  final _cmother = TextEditingController();
  final _ccolor = TextEditingController();
  final _cshoecolor = TextEditingController();
  final _cclothcolor = TextEditingController();
  final _cmothertongue = TextEditingController();
  final _identitySymbol = TextEditingController();
  final _missingfrom = TextEditingController();
  String _cmental = "yes",
      _cvision = "yes",
      _chearing = "yes",
      _cspeaking = "yes";
  DateTime _date = null;
  DateTime _time = null;
  bool _autoValidate = false;
  final _formKey = GlobalKey<FormState>();
  var response_success;
  List<Asset> images = List<Asset>();
  DatabaseReference _ref;
  File f;
  List<File> files = List();
  bool done = false;
  bool done2 = false;
  int uploaded = 0;
  int _mentallyStable = 0;
  int _hearing = 0;
  int _vision = 0;
  int _speaking = 0;
  var gender = ['male', 'female', 'other'];
  String formattedTime = null, formattedDate = null;

  void initState() {
    super.initState();
    _ref = FirebaseDatabase.instance.reference().child('Child_Data');
  }

  _showImageDialog() {
    showDialog(
        context: context,
        builder: (_) => new CupertinoAlertDialog(
              content: new Text("Data Uploaded"),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => SearchImage()));
                  },
                ),
              ],
            ));
  }

  Widget checkupload() {
    if (uploaded == 0)
      return Text(
        "No image Selected",
        style: TextStyle(fontSize: 15, color: Colors.black),
      );
    else if (uploaded == 1)
      return CircularProgressIndicator();
    else
      return Text(
        "Image Selected",
        style: TextStyle(fontSize: 15, color: Colors.black),
      );
  }

  void createData() {
    if (_mentallyStable == 0) {
      _cmental = "yes";
    } else {
      _cmental = "no";
    }
    if (_hearing == 0) {
      _chearing = "yes";
    } else {
      _chearing = "no";
    }
    if (_vision == 0) {
      _cvision = "yes";
    } else {
      _cvision = "no";
    }
    if (_speaking == 0) {
      _cspeaking = "yes";
    } else {
      _cspeaking = "no";
    }
    String child_name = _cusername.text;
    String child_age = _cage.text;
    String child_gender = _cgender.text;
    String last_place = _lastplace.text;
    Map<dynamic, dynamic> child_data = {
      'cname': child_name,
      'cage': child_age,
      'hearing': _chearing,
      'vision': _cvision,
      'speaking': _cspeaking,
      'cgender': child_gender,
      'parent': _cparent.text,
      'education': _ceducation.text,
      'address': _caddress.text,
      'mother': _cmother.text,
      'color': _ccolor.text,
      'shoe_color': _cshoecolor.text,
      'cloth_color': _cclothcolor.text,
      'mother_tongue': _cmothertongue.text,
      'mental_health': _cmental,
      'last_date': formattedDate,
      'last_time': formattedTime,
      'last_place': last_place,
      'Reporting Person Name': name,
      'Reporting Email': email,
      'Reporting Person Phone': parentphone,
      'Identity Symbol': _identitySymbol.text,
      'Missing Place': _missingfrom.text,
    };
    _ref.push().set(child_data);
    _showImageDialog();
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      switch (value) {
        case 0:
          _mentallyStable = value;
          break;
        case 1:
          _mentallyStable = value;
          break;
      }
    });
  }

  void _hearingvalue(int value) {
    setState(() {
      switch (value) {
        case 0:
          _hearing = value;
          break;
        case 1:
          _hearing = value;
          break;
      }
    });
  }

  void _speakingvalue(int value) {
    setState(() {
      switch (value) {
        case 0:
          _speaking = value;
          break;
        case 1:
          _speaking = value;
          break;
      }
    });
  }

  void _visionvalue(int value) {
    setState(() {
      switch (value) {
        case 0:
          _vision = value;
          break;
        case 1:
          _vision = value;
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: HomePageBody());
  }

  Widget HomePageBody() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            //mainAxisAlignment: min,
            children: <Widget>[
              NameInput(),
              SizedBox(
                height: 16,
              ),
              Parent_Name(),
              SizedBox(
                height: 16,
              ),
              Mother_Name(),
              SizedBox(
                height: 16,
              ),
              Row(
                children: <Widget>[
                  Flexible(child: AgeInput()),
                  Flexible(child: Education()),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Flexible(child: GenderInput()),
                  Flexible(child: Mother_tongue()),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Address(),
              SizedBox(
                height: 16,
              ),

              Color(),

              SizedBox(
                height: 16,
              ),

              Row(
                children: [
                  Flexible(child: Shoe_Color()),
                  Flexible(child: Cloth_Color()),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton.icon(
                    label: Text(
                      _date == null ? "Date of Missing" : "$formattedDate",
                      style: TextStyle(color: Colors.black),
                    ),
                    icon: Icon(
                      Icons.calendar_today_rounded,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2018, 3, 5),
                          maxTime: DateTime.now(), onChanged: (date) {
                        print('change $date');
                      }, onConfirm: (date) {
                        print('confirm $date');
                        setState(() {
                          _date = date;
                          formattedDate =
                              DateFormat('dd-MM-yyyy').format(_date);
                        });
                      }, currentTime: DateTime.now(), locale: LocaleType.en);
                    },
                  ),
                  TextButton.icon(
                    label: Text(
                      _time == null ? "Time of Missing" : "$formattedTime",
                      style: TextStyle(color: Colors.black),
                    ),
                    icon: Icon(
                      Icons.access_alarm,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      DatePicker.showTime12hPicker(context,
                          showTitleActions: true, onChanged: (time) {
                        print('change $time');
                      }, onConfirm: (time) {
                        print('confirm $time');
                        setState(() {
                          _time = time;
                          formattedTime = DateFormat('kk:mm:a').format(_time);
                        });
                      }, currentTime: DateTime.now(), locale: LocaleType.en);
                    },
                  )
                ],
              ),

              SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Flexible(child: MissingPlace()),
                  Flexible(child: Last_Spotted()),
                ],
              ),

              SizedBox(
                height: 16,
              ),
              Identity_Symbol(),
              SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Text(
                    "Capacities",
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    'Mentally Stable',
                  ),
                  Row(children: [
                    new Radio(
                      value: 0,
                      groupValue: _mentallyStable,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Yes',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Radio(
                      value: 1,
                      groupValue: _mentallyStable,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'No',
                      style: new TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ])
                ],
              ),
              SizedBox(
                height: 16,
              ),

              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    'Vision',
                  ),
                  Row(children: [
                    new Radio(
                      value: 0,
                      groupValue: _vision,
                      onChanged: _visionvalue,
                    ),
                    new Text(
                      'Yes',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Radio(
                      value: 1,
                      groupValue: _vision,
                      onChanged: _visionvalue,
                    ),
                    new Text(
                      'No',
                      style: new TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ])
                ],
              ),
              SizedBox(
                height: 16,
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    'Hearing',
                  ),
                  Row(children: [
                    new Radio(
                      value: 0,
                      groupValue: _hearing,
                      onChanged: _hearingvalue,
                    ),
                    new Text(
                      'Yes',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Radio(
                      value: 1,
                      groupValue: _hearing,
                      onChanged: _hearingvalue,
                    ),
                    new Text(
                      'No',
                      style: new TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ])
                ],
              ),
              SizedBox(
                height: 16,
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    'Speaking',
                  ),
                  Row(children: [
                    new Radio(
                      value: 0,
                      groupValue: _speaking,
                      onChanged: _speakingvalue,
                    ),
                    new Text(
                      'Yes',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Radio(
                      value: 1,
                      groupValue: _speaking,
                      onChanged: _speakingvalue,
                    ),
                    new Text(
                      'No',
                      style: new TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ])
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.add_circle_outline),
                    color: Colors.black,
                    tooltip: "Add Image",
                    onPressed: pickImages,
                  ),
                  Padding(
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: <Widget>[
                          checkupload(),
                          //Text("Image not Selected")
                          //Text("no Image Selected")
                        ],
                      )),
                ],
              ),

              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                      child: done2
                          ? SizedBox(
                              height: 100.0,
                              child: ListView(
                                // padding: EdgeInsets.all(10),
                                scrollDirection: Axis.horizontal,
                                children: List.generate(images.length, (index) {
                                  Asset asset = images[index];
                                  return Padding(
                                    padding: EdgeInsets.all(5),
                                    child: AssetThumb(
                                      asset: asset,
                                      width: 100,
                                      height: 100,
                                    ),
                                  );
                                }),
                              ),
                            )
                          : SizedBox(
                              height: 20.0,
                            )),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),

              RaisedButton(
                onPressed: _validateInputs,
                color: Colors.black,
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text('Start Search'),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
              // Padding(
              //   padding: EdgeInsets.all(10.0),
              //   child: RaisedButton(
              //       onPressed: () => uploadImageToServer(),
              //       child: new Text('Upload Image To server')),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      if (formattedDate == null) {
        showDialog(
            context: context,
            builder: (_) => new CupertinoAlertDialog(
                  content: new Text("Please select Missing Date"),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Ok'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ));
      } else if (formattedDate == null) {
        showDialog(
            context: context,
            builder: (_) => new CupertinoAlertDialog(
                  content: new Text("Please select Missing Time"),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Ok'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ));
      } else {
        createData();
        //checkdata(_cusername.text);
        _formKey.currentState.save();
      }
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void showInSnackBar(String value) {
    Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(value)));
  }

  Widget NameInput() {
    return TextFormField(
      controller: _cusername,
      cursorColor: Colors.lightGreen,
      decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Child Name',
          labelStyle: TextStyle(color: Colors.black)),
      keyboardType: TextInputType.text,
      validator: (String arg) {
        if (arg.length < 3)
          return 'Name must be more than 2 charater';
        else if (!isAlpha(arg)) {
          return 'Invalid Name';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget AgeInput() {
    return TextFormField(
      controller: _cage,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Age",
        labelStyle: TextStyle(color: Colors.black),
      ),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (isNumeric(arg) == false) {
          return 'Invalid Age';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget GenderInput() {
    return TextField(
      controller: _cgender,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Gender",
        labelStyle: TextStyle(color: Colors.black),
        suffixIcon: PopupMenuButton<String>(
          icon: const Icon(Icons.arrow_drop_down),
          onSelected: (String value) {
            _cgender.text = value;
          },
          itemBuilder: (BuildContext context) {
            return gender.map<PopupMenuItem<String>>((String value) {
              return new PopupMenuItem(child: new Text(value), value: value);
            }).toList();
          },
        ),
      ),
    );
  }

  Widget MissingPlace() {
    return TextFormField(
      controller: _missingfrom,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Lost From which City",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (!isAlpha(arg)) {
          return 'Invalid Place';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Last_Spotted() {
    return TextFormField(
      controller: _lastplace,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Where was s/he last Spotted",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Identity_Symbol() {
    return TextFormField(
      controller: _identitySymbol,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Identity Symbol/شناختی علامت",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Parent_Name() {
    return TextFormField(
      controller: _cparent,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Parent/Guardian Full Name",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (!isAlpha(arg)) {
          return 'Invalid Name';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Mother_Name() {
    return TextFormField(
      controller: _cmother,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Mother Full Name",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (!isAlpha(arg)) {
          return 'Invalid Name';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Education() {
    return TextFormField(
      controller: _ceducation,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Education",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Address() {
    return TextFormField(
      controller: _caddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Address",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Color() {
    return TextFormField(
      controller: _ccolor,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Color",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (isAlphanumeric(arg) == false) {
          return 'Invalid Color';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Shoe_Color() {
    return TextFormField(
      controller: _cshoecolor,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Color of shoes",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (isAlphanumeric(arg) == false) {
          return 'Invalid Shoe Color';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Cloth_Color() {
    return TextFormField(
      controller: _cclothcolor,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Color of Clothes",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (isAlphanumeric(arg) == false) {
          return 'Invalid Cloth Color';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget Mother_tongue() {
    return TextFormField(
      controller: _cmothertongue,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Mother Tongue",
          labelStyle: TextStyle(color: Colors.black)),
      textInputAction: TextInputAction.next,
      validator: (String arg) {
        if (arg == "")
          return 'Please fill this field';
        else if (isAlphanumeric(arg) == false) {
          return 'Invalid Mother Tongue';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Future<void> pickImages() async {
    List<Asset> resultList = List<Asset>();
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: true,
        // imageQuality:100,
        selectedAssets: images,
        materialOptions: MaterialOptions(
          actionBarTitle: "Gallery",
        ),
      );
    } on Exception catch (e) {
      print(e);
    }

    for (int i = 0; i < resultList.length; i++) {
      final path1 =
          await FlutterAbsolutePath.getAbsolutePath(resultList[i].identifier);
      print(path1);
      String dir = (await getApplicationDocumentsDirectory()).path;
      print(dir);
      String child_name = _cusername.text;
      String newPath =
          path.join(dir, '$email' + '_' + '$child_name' + '_' + '$i.jpg');
    
      f = await File((path1)).copy(newPath);
      print(f);
      files.add(f);
    }
    setState(() {
      if (resultList.length == 0) {
        uploaded = 0;
      } else {
        uploaded = 2;

        images = resultList;
      }
      uploadImageToServer(files);
    });
  }

  uploadImageToServer(List<File> image_paths) async {
    print("attempting to connecting to server......");
    print(image_paths.length);
    var uri = Uri.parse(uri2);
    print("connection established.");
    for (int i = 0; i < image_paths.length; i++) {
      final bytes = File(image_paths[i].path).readAsBytesSync();
      var stream = new http.ByteStream(
          DelegatingStream.typed(image_paths[i].openRead()));
      var length = await image_paths[i].length();
      print(length);
      var request = new http.MultipartRequest("POST", uri);

      var multipartFile = new http.MultipartFile(
        'file',
        stream,
        length,
        filename: path.basename(image_paths[i].path),
      );
      request.files.add(multipartFile);
      var response = await request.send();
      response_success = await response.stream.bytesToString();
      if (response.statusCode == 200) {
        final snackBar = SnackBar(content: Text('$response_success'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        setState(() {
          done2 = true;
          done = true;
        });
      } else {
        final snackBar = SnackBar(content: Text('Some Error Occur Try Again'));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        files.clear();
      }
    }
    files.clear();
  }
}

Future<http.Response> createAlbum(String title) {
  return http.post(
    Uri.https('jsonplaceholder.typicode.com', 'albums'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'title': title,
    }),
  );
}
