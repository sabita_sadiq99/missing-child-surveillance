import 'package:flutter/material.dart';


import 'package:validators/validators.dart';

import 'package:flutter/cupertino.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'dart:io';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:path/path.dart' as path;
import "package:http/http.dart" as http;
import './admin.dart';
import 'package:async/async.dart';
import 'package:path_provider/path_provider.dart';
import 'api_link.dart';


class Admin_Upload extends StatefulWidget {
  Admin_Upload1 createState() => Admin_Upload1();
}
class Admin_Upload1 extends State<Admin_Upload> {
  final _child_name = TextEditingController();
   final _formKey = GlobalKey<FormState>();
  int uploaded = 0;
  String child_name ="";
    bool _autoValidate = false;
  File f;
  bool done = false;
  var response_success;
  List<Asset> images = List<Asset>();
  List<File> files = List();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                Text(
                  "Admin Upload",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                ),
              ]),
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
    body: Container(
      padding: EdgeInsets.all(16),
      child: Form( key: _formKey,child: SingleChildScrollView(child: 
      Column(children: <Widget>[
        SizedBox(
          height:16,
        ),
        NameInput(),
         SizedBox(
          height:16,
        ),
               Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.add_circle_outline),
                    color: Colors.black,
                    tooltip: "Add Image",
                    onPressed: pickImages,
                  ),
                  Padding(
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: <Widget>[
                          checkupload(),
                          //Text("Image not Selected")
                          //Text("no Image Selected")
                        ],
                      )),
                ],
              ),
           new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                      child: done
                          ? SizedBox(
                              height: 100.0,
                              child: ListView(
                                // padding: EdgeInsets.all(10),
                                scrollDirection: Axis.horizontal,
                                children: List.generate(images.length, (index) {
                                  Asset asset = images[index];
                                  return Padding(
                                    padding: EdgeInsets.all(5),
                                    child: AssetThumb(
                                      asset: asset,
                                      width: 100,
                                      height: 100,
                                    ),
                                  );
                                }),
                              ),
                            )
                          : SizedBox(
                              height: 20.0,
                            )),
                ],
              ),
                RaisedButton(
                onPressed: ()
                {
                  
                  _validateInputs();
                },
                color: Colors.black,
                textColor: Colors.white,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Text('Submit'),
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
              
      ],),)),
    ));

  }

     _validateInputs() {
    if (_formKey.currentState.validate()) {
        //checkdata(_cusername.text);
        _formKey.currentState.save();
        final snackBar = SnackBar(content: Text('Uploading Please wait'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
     createImageFolderinServer();
      
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
        
      });
    }
  }


Widget NameInput() {
    return TextFormField(
      controller: _child_name,
      cursorColor: Colors.lightGreen,
      decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Child Name',
          labelStyle: TextStyle(color: Colors.black)),
      keyboardType: TextInputType.text,
      validator: (String arg) {
        print(arg.length);
        if (arg.length == 0)
          return 'Name must be more than 2 charater';
        else if (isNumeric(arg)) {
          return 'Invalid Name';
        } else
          return null;
      },
      
    );
  }
    Widget checkupload() {
      print(uploaded);
    if (uploaded == 0)
      return Text(
        "No image Selected",
        style: TextStyle(fontSize: 15, color: Colors.black),
      );
    else if (uploaded == 1)
      return CircularProgressIndicator();
    else
      return Text(
        "Image Selected",
        style: TextStyle(fontSize: 15, color: Colors.black),
      );
  }
   Future<void> pickImages() async {
     print('$child_name');
     child_name = _child_name.text;
    List<Asset> resultList = List<Asset>();
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 1,
        enableCamera: true,
        // imageQuality:100,
        selectedAssets: images,
        materialOptions: MaterialOptions(
          actionBarTitle: "Gallery",
        ),
      );
    } on Exception catch (e) {
      print(e);
    }
  print("Length  $resultList.length");
    for (int i = 0; i < resultList.length; i++) {
      final path1 =
          await FlutterAbsolutePath.getAbsolutePath(resultList[i].identifier);
      print("This is path"+path1);
      String dir = (await getApplicationDocumentsDirectory()).path;
      String newPath = path.join(dir, '$child_name'+'_'+'$i.jpg');
      f = await File((path1)).copy(newPath);
      // print(f);
      files.add(f);
    }
    setState(() {
      if(resultList.length == 0)
      {
        uploaded =0;
        done = false;
      }
      else{
        uploaded =2;
        
      images = resultList;
      
        done=true;
      }
      
    
     
     
    });
   
  }
  Future<void> createImageFolderinServer() async
{
   print("attempting to connecting to server......");
    final response = await http.get(
    Uri.parse(uri2+'/hitpoint'),
    
  );  
  if(response.statusCode == 200)
  {
    uploadImageToServer(files);
  }
}
   uploadImageToServer(List<File> image_paths) async {
     
    print("attempting to connecting to server......");
   var uri = Uri.parse( uri2+'/admin');
      
       dynamic lst = new List(image_paths.length);


    for(int i=0;i<image_paths.length;i++)
    {
      
      var stream = new http.ByteStream(DelegatingStream.typed(image_paths[i].openRead()));
      var length = await image_paths[i].length();
      var request = new http.MultipartRequest("POST", uri);
       var multipartFile = new http.MultipartFile('file', stream, length,filename: path.basename(image_paths[i].path), );
       request.files.add(multipartFile);
       var response = await request.send();
       response_success = await response.stream.bytesToString();
       if(response.statusCode ==  200)
       {
         lst[i] = true; 
       }
       else
       {
         lst[i] = false;
       }
    }
   
      if(lst.contains(false))
      {
          final snackBar = SnackBar(content: Text('Some Error Occur Try Again'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
      else{
          final snackBar = SnackBar(content: Text('$response_success'));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
           Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MainPage(
           
          ),
        ));

      }
    
    files.clear();
   
    
  }
}