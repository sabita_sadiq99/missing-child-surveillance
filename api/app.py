from flask import Flask, flash, jsonify, render_template, request, redirect, url_for, Blueprint
import os,shutil
from werkzeug.utils import secure_filename
from datetime import date
import psycopg2
import json
import time
import schedule
import operator
import base64
from crop_fc import sdr
from utils import deepFaceFun
import cv2
from mtcnn import MTCNN

conn = psycopg2.connect(database="fyp",user="postgres",password="fast1234",host = "localhost", port = "5432")
cur = conn.cursor()
app = Flask(__name__)
today = date.today()
static ="/Users/apple/Desktop/development/sign_in_flutter/api/static"
api = "/Users/apple/Desktop/development/sign_in_flutter/api"
count=""
data =''
def get_directory(filename):
    x = filename.rsplit("_")
    return(x[0])
def get_date():
    d1 = today.strftime("%d_%m_%Y")
    return(d1)
def get_index(filename):
    x = filename.rsplit("_",1)
    x1 = x[1].rsplit(".")
    return(x1[0])

@app.route('/hitpoint', methods = ['GET','POST'])
def getMenuData():  
    parent_dir = static + "/train_dir"
    totalFiles = 0
    totalDir = 0
    for base, dirs, files in os.walk(parent_dir):
        print('Searching in : ',base)
        for directories in dirs:
            totalDir += 1
    print(totalDir)
    totalDir =totalDir +1
    directory = "image"+str(totalDir)
    path = os.path.join(parent_dir,directory)
    os.makedirs(path)
    
    
  
    return 'ok'

@app.route('/admin',methods=['GET', 'POST'])

def upload_admin():
    if request.method == 'POST':
        if 'file' not in request.files:
            return 'No file found'
        file = request.files['file']
        if file.filename == '':
            return 'file name not found'
        if file:
            alist={}
            now = time.time()
            directory=static + "/train_dir/"
            os.chdir(directory)
            for file1 in os.listdir("."):
                if os.path.isdir(file1):
                    timestamp = os.path.getmtime( file1 )
            # get timestamp and directory name and store to dictionary
                    alist[os.path.join(os.getcwd(),file1)]=timestamp

    # sort the timestamp 
            for i in sorted(alist.items(), key=operator.itemgetter(1)):
                latest="%s" % ( i[0])
    # latest=sorted(alist.iteritems(), key=operator.itemgetter(1))[-1]
            l = latest.split("/")
            l_gan=l[len(l)-1]
            path_gan = 'python3 ' + api +'/test.py ' + l_gan
            filename = secure_filename(file.filename)
            file.save(os.path.join(latest, filename))
            sdr(latest)
            os.system(path_gan)
            # # global fileTest
            # fileTest = filename
    return 'Uploaded'
@app.route('/', methods=['GET', 'POST'])
def upload_file1():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return 'No file found'
        file = request.files['file']
        if file.filename == '':
            return 'file name not found'
        if file:
           # print(file.filename)
            index = get_index(file.filename)
            directory = get_directory(file.filename)
            directory = directory + get_date()
            parent_dir = static + "/uploads/"
            path = os.path.join(parent_dir,directory)
            if index == "0":
                if os.path.exists(path):
                    print("file already exist")
                else: 
                    os.makedirs(path)
            filename = file.filename
            file.save(os.path.join(path, filename))
            final_path =os.path.join(path, filename)
            img = cv2.imread(final_path)
            detector = MTCNN()
            detections = detector.detect_faces(img)
            for detection in detections:
                score = detection["confidence"]
                if score >= 0.90:
                    x, y, w, h = detection["box"]
                    detected_face = img[int(y):int(y+h), int(x):int(x+w)]
                    os.remove(final_path)
                    cv2.imwrite(os.path.join(final_path), detected_face)
            my_string=""
            with open(final_path,"rb") as image:
                mat = base64.b64encode(image.read())
                my_string = mat.decode('utf-8')
            # obal fileTest
            fileTest = os.path.join(path, filename)
            x = fileTest.split("/")
            y = x[len(x)-1]
            z = y.split("_")
            cur.execute(f"INSERT INTO public.uploads (name, email, url,report_status,base64_encoding) VALUES ('{z[1]}','{z[0]}','{fileTest}','false','{my_string}') ")
            conn.commit()
            # eturn redirect(url_for('upload_file2'))
    return 'Uploaded'

@app.route('/login',methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.args.get('email')
        postgreSQL_select_Query = "select COUNT(urltest) from public.matched where email = %s"
        cur.execute(postgreSQL_select_Query, (email,))
        count = cur.fetchone()
        print(count)
        if count[0] == 0:
           check = "false"
        else:
           check = "true"
    return check

@app.route('/upload') #hitpoint to get count of the uploads by the admin
def get_upload():
        postgreSQL_select_Query = "select COUNT(distinct name) from public.uploads"
        cur.execute(postgreSQL_select_Query)
        count = cur.fetchone()
        print(count)
        return (str(count[0]))
        # if count[0] == 0:
        #     return  "false"
        # else:
        #     return  "true" 
@app.route('/delete',methods=['GET', 'POST']) #delete the wrong matched data from the matched
def get_deleted():
        my_id = request.args.get('id')
        cur.execute("DELETE FROM public.matched WHERE id = %s", (my_id,))
        conn.commit()
        return 'deleted'
        # if count[0] == 0:
        #     return  "false"
        # else:
        #     return  "true" 
@app.route('/matched',methods=['GET', 'POST']) #hitpoint to get the matched for notification of a single user
def get_matchhed_image():
    email = request.args.get('email')
    postgreSQL_select_Query1 = "select Distinct on (name) id,name,email from public.matched where email = %s"
    cur.execute(postgreSQL_select_Query1, (email,))
    data = cur.fetchall()
    return jsonify({
        "url" : data
    })
@app.route('/getall',methods=['GET', 'POST']) #hitpoint to get images of a particular user by name
def get_all():
    
       #name = request.args.get('name')
    postgreSQL_select_Query1 = "select DISTINCT ON (name) name,base64_encoding,email from public.uploads where report_status=false"
    cur.execute(postgreSQL_select_Query1)
    data = cur.fetchall()
    return jsonify({
        "url" : data
    })
@app.route('/geturls',methods=['GET', 'POST']) #hitpoint to get images of a particular user by name
def get_urls():
    
    name = request.args.get('name')
    postgreSQL_select_Query1 = "select base64_encoding from public.uploads where report_status=false AND name= %s"
    cur.execute(postgreSQL_select_Query1, (name,))
    data = cur.fetchall()
    return jsonify({
        "url" : data
    })
        # if count[0] == 0:
        #     return  "false"
        # else:
        #     return  "true" 
@app.route('/update',methods=['GET', 'POST']) #hitpoint to get images of a particular user by name
def update_status():
    
    name = request.args.get('name')
    email = request.args.get('email')
    url1 = request.args.get('submit')
    postgreSQL_select_Query1 = "update public.uploads set report_status = true where name=%s AND email=%s"
    cur.execute(postgreSQL_select_Query1, (name,email,))
    cur.execute("DELETE FROM public.matched WHERE name = %s AND email=%s AND urltest=%s", (name,email,url1))
    conn.commit()
    return 'updated'
@app.route('/getstatus',methods=['GET', 'POST']) 
def get_status():
    postgreSQL_select_Query1 = "select distinct on (name) name,url,email,report_status from public.uploads"
    cur.execute(postgreSQL_select_Query1)
    data = cur.fetchall()
    return jsonify({
         "url" : data
    })
@app.route('/getreport',methods=['GET', 'POST']) 
def get_report():
    name = request.args.get('name')
    postgreSQL_select_Query1 = "select urltest,base64_encoding_sub,base64_encoding_mat from public.matched where name = %s"
    cur.execute(postgreSQL_select_Query1, (name,))
    data = cur.fetchall()
    return jsonify({
         "url" : data
    })
@app.route('/wrong',methods=['GET', 'POST']) 
def wrong_report():
    name = request.args.get('name')
    email = request.args.get('email')
    url1 = request.args.get('submit')
    print(name)
    print(email)
    print(url1)
    cur.execute("DELETE FROM public.matched WHERE name = %s AND email=%s AND urltest=%s", (name,email,url1))
    cur.execute("DELETE FROM public.final_match WHERE name = %s AND email=%s AND submit_url=%s", (name,email,url1))
    conn.commit()
    return 'ok'
@app.route('/job',methods=['GET', 'POST']) 
def job():
    matchedval =[]
    cur.execute('select * from public.uploads where report_status = false')
    data = cur.fetchall()
    i=0
    j=0
    while i < len(data):
        sub=""
        mat=""
        with open(data[i][3],"rb") as image:
            sub = base64.b64encode(image.read())
            sub = sub.decode('utf-8')
        x = (data[i][3].split('/'))
        y= len(x)
        directory = x[y-2]
        fileTest = x[y-1]
        name = fileTest.split('_')[1]
        email = fileTest.split('_')[0]
        print(data[i][3])
        res1 = deepFaceFun(data[i][3])
        print(res1)
        matchedval.append(res1['identity'])
        if(len(matchedval[i]))!=0:
            if len(matchedval[i]) >= 5:
                for j in range(5):
                    print(matchedval[i][j])
                    with open(matchedval[i][j],"rb") as image:
                        mat = base64.b64encode(image.read())
                        mat= mat.decode('utf-8')
                    cur.execute(f"INSERT INTO public.matched(name,email,urltest,matched_url,base64_encoding_sub,base64_encoding_mat) VALUES ('{name}','{email}','{data[i][3]}','{matchedval[i][j]}','{sub}','{mat}') ")
                    cur.execute(f"INSERT INTO public.final_match (name,email,submit_url,match_dir) VALUES ('{name}','{email}','{data[i][3]}','{matchedval[i][j]}') ")
            elif len(matchedval[i]) < 5:
                for j in range(len(matchedval[i])):
                    with open(matchedval[i][j],"rb") as image:
                        mat = base64.b64encode(image.read())
                        mat = mat.decode('utf-8')
                    #print(matchedval[i][j])
                    cur.execute(f"INSERT INTO public.matched(name,email,urltest,matched_url,base64_encoding_sub,base64_encoding_mat) VALUES ('{name}','{email}','{data[i][3]}','{matchedval[i][j]}','{sub}','{mat}') ")
                    cur.execute(f"INSERT INTO public.final_match (name,email,submit_url,match_dir) VALUES ('{name}','{email}','{data[i][3]}','{matchedval[i][j]}') ")
            else:
                with open(matchedval[0][0],"rb") as image:
                    mat = base64.b64encode(image.read())
                    mat = mat.decode('utf-8')
                #print(matchedval[0][0])
                cur.execute(f"INSERT INTO public.matched(name,email,urltest,matched_url,base64_encoding_sub,base64_encoding_mat) VALUES ('{name}','{email}','{data[i][3]}','{matchedval[0][0]}','{sub}','{mat}') ")
                cur.execute(f"INSERT INTO public.final_match (name,email,submit_url,match_dir) VALUES ('{name}','{email}','{data[i][3]}','{matchedval[0][0]}') ")
        conn.commit()
        i = i + 1
    return 'ok'
    
schedule.every().day.at("22:00").do(job)

if __name__ == "__main__":
    app.run(host='192.168.0.146', port=8000, debug=True)
